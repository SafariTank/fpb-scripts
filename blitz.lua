--[[
Blitz helper v0.2 by h0nda

When Hotkey is pressed tryes to Q target

]]
--[[        Config      ]]
RANGE = 900 

HK=32 -- spacebar
scriptActive = false
spellSlot = SPELL_SLOT_1

function altDoFile(name)
        dofile(debug.getinfo(1).source:sub(debug.getinfo(1).source:find(".*\\")):sub(2)..name)end
altDoFile("target_selector.lua")
altDoFile("vector.lua")
altDoFile("linear_prediction.lua")

lp = LinearPrediction:new(RANGE,1.5)
ts = TargetSelector:new(TARGET_NEAR,RANGE)

function Timer( tick )
    ts:tick()
    lp:tick()
    if scriptActive == true then
        
        if ts.target~=nil and player:CanUseSpell(spellSlot)then
            predicted = lp:getPredictionFor(ts.target.name)
            if predicted then
                player:UseSpell(spellSlot,predicted.x, predicted.z)
            end
        end 
    end

end
function Drawer()
    DrawCircle(RANGE,player.x, player.y, player.z)
    if ts.target ~= nil then
        DrawCircle(100,ts.target.x, ts.target.y, ts.target.z)
    end
    for k,v in pairs(lp.prediction) do 
        DrawCircle(30,v.x,player.y,v.z)
        
    end

end
function Hotkey(msg,key)
    if msg == KEY_DOWN then
        if key == HK then
            scriptActive = true
        end
    else
        if key == HK then
            scriptActive = false
        end
    end
end
function Load()
    if player.charName ~= "Blitzcrank" then 
        script:Unload()
    else
        script.keyCallback = "Hotkey"
        script.timerCallback = { name = "Timer", interval = 100 }--ms 
        script.drawCallback = "Drawer"
        PrintChat(" Blitz helper v0.2 by h0nda")
    end

end
