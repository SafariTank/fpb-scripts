--[[
"Ezreal combo helper by h0nda/jedi

Hold spacebar to win :)

]]

HK=32 --spacebar
scriptActive = false

combo = { SPELL_SLOT_1,SPELL_SLOT_3,SPELL_SLOT_2,SPELL_SLOT_1}

spell_index = 1
tmp_target = nil
range = 900
proj_speed = 1.2

function altDoFile(name)
    dofile(debug.getinfo(1).source:sub(debug.getinfo(1).source:find(".*\\")):sub(2)..name)end
    altDoFile("target_selector.lua")
    altDoFile("vector.lua")
    altDoFile("linear_prediction.lua")


    lp = LinearPrediction:new(range,proj_speed)
    ts = TargetSelector:new(TARGET_LOW_HP,range)
    attack_finished = true

    function Timer(tick)


       ts:tick()
       lp:tick()
       if(attack_finished==false) then
        return
    end
    if(scriptActive) then
     

   if(ts.target ~= nil )then
    if player:CanUseSpell(combo[spell_index])  then
        p = lp:getPredictionFor(ts.target.name)
        if(p) then
            player:UseSpell(combo[spell_index], p.x,p.z)
            spell_index=spell_index+1
            if combo[ spell_index ] == nil then
                spell_index = 1
            end  
        end

    end


end
end
end

function Drawer()
	DrawCircle(range,player.x, player.y, player.z)
    if ts.target ~= nil then
        DrawCircle(100,ts.target.x, ts.target.y, ts.target.z)
    end
end
function Hotkey(msg,key)
    if msg == KEY_DOWN then
        --key down
        if key == HK then
           scriptActive = true
       end
   else
    if key == HK then
        spell_index=1
        scriptActive = false
    end
end
end
function Remove(obj)
 if(string.find(obj.name,"Ezreal_basicattack")) then
     attack_finished = true
 end
end
function Load()
    if player.charName ~= "Ezreal" then 
        script:Unload()
        return
    end
    script.keyCallback = "Hotkey"
    script.timerCallback = { name = "Timer", interval = 100 }--ms 
    script.drawCallback = "Drawer"
    script.deleteObjectCallback = "Remove"
    PrintChat("Ezreal combo helper by h0nda/jedi")

end