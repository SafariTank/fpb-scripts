--[[
Jarvan helper v0.2 by h0nda

press spacebar to hit weakest enemy by flag, and drag yourself to it
if flag is somware near spacebar will simply drag you to it

]]
HK=32 -- spacebar
scriptActive = false
range = 950
stand = nil

function altDoFile(name)
        dofile(debug.getinfo(1).source:sub(debug.getinfo(1).source:find(".*\\")):sub(2)..name)end
altDoFile("target_selector.lua")
altDoFile("vector.lua")
altDoFile("linear_prediction_const_t.lua")


ts = TargetSelector:new(TARGET_LOW_HP,range)
lp = LinearPredictionConstT:new(range,300)

function Timer(tick)

	ts:tick()
    lp:tick()

	if(scriptActive) then

		if(stand~=nil and player:GetDistanceTo(stand)<range) then
          if player:CanUseSpell(SPELL_SLOT_1) == STATE_READY then
            player:UseSpell(SPELL_SLOT_1, stand.x, stand.z)
        end
    else
        
        if player:CanUseSpell(SPELL_SLOT_3) == STATE_READY and ts.target~=nil then
            p = lp:getPredictionFor(ts.target.name)
            if p ~= nil then
                player:UseSpell(SPELL_SLOT_3,p.x,p.z)
            end
        end
    end
    
end
end

function Drawer()
	DrawCircle(range,player.x, player.y, player.z)
    if ts.target ~= nil then
        DrawCircle(100,ts.target.x, ts.target.y, ts.target.z)
        p =lp:getPredictionFor(ts.target.name)
        if p~=nil then
            DrawCircle(30,p.x, ts.target.y, p.z)
        end
    end
end
function Hotkey(msg,key)
    if msg == KEY_DOWN then
        --key down
        if key == HK then
            if scriptActive == false then
         end
         scriptActive = true
     end
 else
    if key == HK then
        scriptActive = false
    end
end
end
function Add( obj )
    
    if string.find( obj.name, "DemacianStandard_flag.troy")  then
       stand = obj
       
   end
end

function Remove( obj )
    if(stand==nil) then
        return
    end
    if obj~=nil and string.find(obj.name,"DemacianStandard_flag.troy") then
        stand = nil
    end
end


function Load()
    if player.charName ~= "JarvanIV" then 
        script:Unload()
        return
    end
    script.createObjectCallback = "Add"
    script.deleteObjectCallback = "Remove"
    script.keyCallback = "Hotkey"
    script.timerCallback = { name = "Timer", interval = 100 }--ms 
    script.drawCallback = "Drawer"
    PrintChat(" Jarvan helper 0.1 by honda")

end