--[[
Ryze spell spamm v0.2 by h0nda/Zynox

Hold spacebar to win :)

]]

HK=32 --spacebar
scriptActive = false

comboHigh = {SPELL_SLOT_1, SPELL_SLOT_2,SPELL_SLOT_1,SPELL_SLOT_3,SPELL_SLOT_1}
comboLow = {SPELL_SLOT_1, SPELL_SLOT_2,SPELL_SLOT_3,SPELL_SLOT_1}

spell_index = 1
range = 650

player.cdr=0.4 --TODO remove this after patch

function altDoFile(name)
        dofile(debug.getinfo(1).source:sub(debug.getinfo(1).source:find(".*\\")):sub(2)..name)end
altDoFile("target_selector.lua")

ts = TargetSelector:new(TARGET_LOW_HP,range)

function Timer(tick)
    if player.dead == true then return end
    
    ts:tick()
    
    if(scriptActive) then
        local myCombo = nil
        if player.cdr < 0.3 then
            myCombo = comboLow
        else
            myCombo = comboHigh
        end
        if(ts.target ~= nil and player:CanUseSpell(myCombo[spell_index])) then
            player:UseSpell(myCombo[spell_index], ts.target)
            spell_index = spell_index + 1
            if myCombo[ spell_index ] == nil then
                spell_index = 1
            end         
        end

        
    end
end

function Drawer()
    if player.dead == true then return end
    DrawCircle(range,player.x, player.y, player.z)
    if ts.target ~= nil then
        DrawCircle(100,ts.target.x, ts.target.y, ts.target.z)
    end
end
function Hotkey(msg,key)
    if msg == KEY_DOWN then
        --key down
        if key == HK then
            scriptActive = true
        end
    else
        if key == HK then
            scriptActive = false
        end
    end
end
function Load()
    if player.charName ~= "Ryze" then 
        script:Unload()
        return
    end
    script.keyCallback = "Hotkey"
    script.timerCallback = { name = "Timer", interval = 100 }--ms 
    script.drawCallback = "Drawer"
    PrintChat(" Ryze skill spamm 0.2 by h0nda/Zynox")

end