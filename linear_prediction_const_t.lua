--[[
    Prediction calculation example v0.1
    Written by h0nda
]]
-- linear prediction with constant time
LinearPredictionConstT = {}

function LinearPredictionConstT:new(range, time)
    local object = { range = range, time = time, memory={}, prediction={}}
    setmetatable(object, { __index = LinearPredictionConstT })  
    return object
end

function LinearPredictionConstT:check_if_target(obj)
   return obj ~= nil and  obj.team == TEAM_ENEMY and obj.dead == false and obj.visible == true
end

function LinearPredictionConstT:tick()
   local count = GetPlayerCount()
   local time = GetTick()
   local position, v

    for i = 1, count, 1 do
        local object = GetPlayer(i) 
        if self:check_if_target(object)  and (player:GetDistanceTo(object)<=self.range or self.range ==0 )then
            if self.memory[object.name] then
               position =  Vector:new(object.x,object.z)
                v = position:sub(self.memory[object.name]):mult(1/(time-self.memory[object.name].t)) 
                self.prediction[object.name]=position:add(v:mult(self.time))
            end
            self.memory[object.name]={x=object.x,z=object.z,t=GetTick()}
        else
            self.prediction[object.name]=nil
        end
    end
end

function LinearPredictionConstT:getPredictionFor(champ_name)
    return self.prediction[champ_name]
end




