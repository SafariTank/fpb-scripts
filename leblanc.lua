scriptActive = false
HK=32
particle = nil
tt = 0

function altDoFile(name)
        dofile(debug.getinfo(1).source:sub(debug.getinfo(1).source:find(".*\\")):sub(2)..name)end

altDoFile("target_selector.lua")
altDoFile("vector.lua")
altDoFile("linear_prediction.lua")
altDoFile("combos_lib.lua")

function simple_test(combo)
    for k,v in pairs(combo) do
        if (v.type~=SPELL_TYPE_USERFUNC and player:CanUseSpell(v.slot)~=STATE_READY) then
            return false
        end
    end
    return true
end

function waitForParticle(target)
    if particle and target and target:GetDistanceTo(particle)<10 then
        return true
    end
    return false
    
end

Q = Spell:new(SPELL_TYPE_NORMAL,SPELL_SLOT_1)
W = Spell:new(SPELL_TYPE_NORMAL,SPELL_SLOT_2)
WS = Spell:new(SPELL_TYPE_SELF,SPELL_SLOT_2)
E = Spell:new(SPELL_TYPE_SKILLSHOT,SPELL_SLOT_3)
R = Spell:new(SPELL_TYPE_NORMAL,SPELL_SLOT_4)
D = Spell:new(SPELL_TYPE_USERFUNC,waitForParticle)

--create new ComboSelector
cr = ComboSelector:new()
--setup combos
cr:add_combo(Combo:new({W,Q,R,D,E},1200,simple_test))
cr:add_combo(Combo:new({W,Q,D,E},1200,simple_test))
cr:add_combo(Combo:new({Q,D,W,WS},650,simple_test))
cr:add_combo(Combo:new({Q,D,E},650,simple_test))
cr:add_combo(Combo:new({Q,E},650,simple_test))
cr:add_combo(Combo:new({E},900,simple_test))


--get most strong awayilable combo

lp = LinearPrediction:new(1200,1.1)
ts = TargetSelector:new(TARGET_LOW_HP,1200)


current_combo = nil
function Timer( tick )
    if player.dead == true then return end
    lp:tick()
    if(scriptActive) then
        if(current_combo==nil) then
                
                
                if(current_combo)then
                    ts.range=current_combo.range
                end
                ts:tick()

               
        else
            if(ts.target) then
                current_combo:runNextSpell(ts.target,lp:getPredictionFor(ts.target.name))
            end
            if(current_combo.finished) then
                current_combo = nil

            end
        end
    else
         current_combo = cr:get_combo()
         if(current_combo)then
                    ts.range=current_combo.range
                end
       ts:tick() 
    end
end

function Drawer()
    if player.dead == true then return end
    if(current_combo) then
        DrawCircle(current_combo.range,player.x, player.y, player.z)
        DrawText(""..current_combo:to_s(),WINDOW_W-200,WINDOW_H/2,1,0,0,1)
    end
    if ts.target ~= nil then
        DrawCircle(100,ts.target.x, ts.target.y, ts.target.z)
         p = lp:getPredictionFor(ts.target.name)
        if p then
            DrawCircle(30,p.x, ts.target.y, p.z)
        end
    end
end
function Hotkey(msg,key)
    if msg == KEY_DOWN then
        --key down
        if key == HK then
            scriptActive = true
        end
    else
        if key == HK then
            scriptActive = false
            if(current_combo) then
                current_combo.finished=false
                current_combo.index=1
                current_combo = nil
            end
        end
    end
end
function Remove(obj)
    if(obj.name:find("displace_AOE_tar")) then
        particle = nil
    end
end
function Add(obj)
    if(obj.name:find("displace_AOE_tar")) then
        particle = obj
    end
end
function Load()
    if player.charName ~= "Leblanc" then 
        script:Unload()
        return
    end
    script.createObjectCallback = "Add"
    script.deleteObjectCallback = "Remove"
    script.keyCallback = "Hotkey"
    script.timerCallback = { name = "Timer", interval = 100 }--ms 
    script.drawCallback = "Drawer"
    PrintChat(" Leblanc helper 0.1 by h0nda")

end
-- leBlanc_displace_AOE_tar.troy
-- leBlanc_displace_AOE_tar_ult.troy
-- leBlanc_shackle_tar.troy

