--[[
Annie Ownage v0.1 by Weee/h0nda

Hold spacebar to win :) (c) h0nda
]]

-- Config:
HK = 32 --spacebar
range = 650
drawRange = false


-- Globals:
ultCombo = {SPELL_SLOT_1,SPELL_SLOT_2,SPELL_SLOT_3}
spell_index = 1
scriptActive = false

StunReadyFlag = false

function altDoFile(name)
    dofile(debug.getinfo(1).source:sub(debug.getinfo(1).source:find(".*\\")):sub(2)..name)
end
altDoFile("target_selector.lua")

ts = TargetSelector:new(TARGET_LOW_HP,range)

function Timer(tick)
    if player.dead or not scriptActive then return end
    ts:tick()
    if ts.target ~= nil then
        if player:CanUseSpell(SPELL_SLOT_4) == STATE_READY then
            if player:CanUseSpell(ultCombo[spell_index]) == STATE_READY then
                if ultCombo[spell_index] == SPELL_SLOT_1 then
                    player:UseSpell(ultCombo[spell_index], ts.target)
                    spell_index = spell_index + 1
                end
                if ultCombo[spell_index] == SPELL_SLOT_2 and player:CanUseSpell(SPELL_SLOT_1) == STATE_COOLDOWN then
                    player:UseSpell(ultCombo[spell_index], ts.target.x, ts.target.z)
                    spell_index = spell_index + 1
                end
                if ultCombo[spell_index] == SPELL_SLOT_3 and player:CanUseSpell(SPELL_SLOT_2) == STATE_COOLDOWN then
                    if not StunReadyFlag then
                        player:UseSpell(ultCombo[spell_index])
                    end
                    spell_index = spell_index + 1
                end
                if ultCombo[ spell_index ] == nil then
                    spell_index = 1
                end
            end
            if StunReadyFlag then
                player:UseSpell(SPELL_SLOT_4, ts.target.x, ts.target.z)
            end
        else
            if player:CanUseSpell(SPELL_SLOT_1) == STATE_READY then player:UseSpell(SPELL_SLOT_1, ts.target) end
            if player:CanUseSpell(SPELL_SLOT_2) == STATE_READY then player:UseSpell(SPELL_SLOT_2, ts.target.x, ts.target.z) end
        end
    end
end

function Drawer()
    if player.dead then return end
    DrawCircle(range,player.x, player.y, player.z)
    if ts.target ~= nil then
        DrawCircle(100,ts.target.x, ts.target.y, ts.target.z)
    end
end

function Hotkey(msg,key)
    if key == HK then
        if msg == KEY_DOWN then
            scriptActive = true
        else
            scriptActive = false
            spell_index = 1
        end
    end
end

function StunReady(object)
    if object ~= nil and object.name == "Data\\Particles\\StunReady.troy" and player:GetDistanceTo(object) == 0 then
        StunReadyFlag = true
        PrintChat("Stun Ready!")
    end
end

function StunGone(object)
    if object ~= nil and object.name == "Data\\Particles\\StunReady.troy" and player:GetDistanceTo(object) == 0 then
        StunReadyFlag = false
        PrintChat("Stun gone!")
    end
end

function Load()
    if player.charName ~= "Annie" then 
        script:Unload()
        return
    end
    script.keyCallback = "Hotkey"
    script.createObjectCallback = "StunReady"
    script.deleteObjectCallback = "StunGone"
    script.timerCallback = { name = "Timer", interval = 10 } --ms 
    if drawRange then script.drawCallback = "Drawer" end
    PrintChat(" >> Annie Ownage v0.1 by Weee/h0nda")
end