LinearPrediction = {}

function LinearPrediction:new(range, proj_speed)
    local object = { range = range, proj_speed = proj_speed, memory={}, prediction={}}
    setmetatable(object, { __index = LinearPrediction })  
    return object
end

function LinearPrediction:check_if_target(obj)
   return obj ~= nil and  obj.team == TEAM_ENEMY and obj.dead == false and obj.visible == true
end

function LinearPrediction:tick()
   local count = GetPlayerCount()
   local time = GetTick()
   local position, v

    for i = 1, count, 1 do
        local object = GetPlayer(i) 
        if self:check_if_target(object)  and (player:GetDistanceTo(object)<=self.range or self.range ==0 )then
            if self.memory[object.name] then
               position =  Vector:new(object.x,object.z)
                v = position:sub(self.memory[object.name]):mult(1/(time-self.memory[object.name].t)) 
                self.prediction[object.name]=position:add(v:mult(player:GetDistanceTo(object)/self.proj_speed))
            end
            self.memory[object.name]={x=object.x,z=object.z,t=GetTick()}
        else
            self.prediction[object.name]=nil
        end
    end
end

function LinearPrediction:getPredictionFor(champ_name)
    return self.prediction[champ_name]
end

