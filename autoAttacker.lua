--[[
	AutoAttacker!
	v0.1

	Smash "X" to win! Allows you to attack-move weakest champion in desireable range.
]]

hotkey = 88 			-- hotkey for autoattacking. 88 - "xXx"
additionalrange = 300 	-- additional range to scan around your champ (your auto-attack range + additionalrange).

function altDoFile(name)
	dofile(debug.getinfo(1).source:sub(debug.getinfo(1).source:find(".*\\")):sub(2)..name)
end

altDoFile("target_selector.lua")

ts = TargetSelector:new(TARGET_LOW_HP,player.range + additionalrange,DAMAGE_PHYSICAL)

function AutoAttacker(msg, keycode)
	if msg == KEY_DOWN or keycode ~= hotkey then return end
	ts.range = player.range + additionalrange
	ts:tick()
	if ts.target ~= nil then player:Attack(ts.target) end
end

function Load()
    script.keyCallback = "AutoAttacker"
    PrintChat(" >> AutoAttacker!")
end