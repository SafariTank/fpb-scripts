-- vector
Vector = {}        
function Vector:new(x, z)  
    local object = { x = x, z = z}
    setmetatable(object, { __index = Vector })  
    return object
end
function Vector:magnitude()  
    return math.sqrt(self.x^2 + self.z^2 )
end

function Vector:normalize()
	return self:mult(1/self:magnitude())
end
function Vector:dot(v)
    return self.x*v.x+self.z*v.z
end

function Vector:cross(v)
    return self.x*v.z-self.z*v.x
end

function Vector:add(v)
    return Vector:new(self.x+v.x, self.z+v.z)
end

function Vector:sub(v)
    return Vector:new(self.x-v.x, self.z-v.z)
end

function Vector:neg()

    return Vector:new(-self.x, -self.z)
end

function Vector:mult(n)
    return Vector:new(self.x*n, self.z*n)
end

function Vector:to_s()
    return "V["..self.x..", "..self.z.."]"
end

